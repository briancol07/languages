# Stunde 6 

| word   | meaning |
| ------ | ------- |
| Stadt  | Cuidad  |
| Land   | Pais    |
| Fluss  | Rio     |
| Punkte | Puntos  |
| Müde   | Cansado |
| Herz   | Corazon |

1. wir machen gruppen arbieit 
2. ihr schreibt 

* Was ist ein souvenir 
* Das ist ein Souvenir

## Adjetive 

Wir markieren die adjektives 

| word        | meaning  |
| ----------- | -------- |
| Lusting     | Gracioso |
| Witzig      | Chiste   |
| Langweiling | Aburrido |
| Schön       | lindo    |
| Hässlich    | Feo      |
| Lecker      | Rico     |
| Müde        | Cansado  |
| Munter      | Energico |

* Sie ist lecker 
* Nicht lecker / Eklig 
* Das ist ein Auto es is 
* Der Bär / Die Bären / Die Bärchen
* Woher kommt der Fußball 
* unentischeden
* Ein gummirbärchenhery (Corazon de gomita)

| Noun           | --                                   |
| -------------- | ------------------------------------ |
| Der Schlüsell  | ein schlüssel <br> kein schlüssel    |
| Das Handy      | ein Handy<br> kein Handy             |
| Die Schokolade | eine Schokolade<br> keine schokolade |
| Die stifte     | das ist Stifte<br> Keine stifte      |
| Das Foto       | Das ist Fotos<br> keine Fotos        |
| Die Flaschen   | Das ist Flaschen <br>keine Flaschen  |

keine frage 

* Habt ihr fragen ? nein, keine freagen 
* (Die Frage/Die Fragen -> Keine Fragen )

/ der Hunger 
ich habe hunger 

| word        | meaning     |
| ----------- | ----------- |
| Hungrig     | Hambre      |
| Dursting    | Sed         |
| kuchen      | pastel      |
| Turnschuhe  | Pantufla    |
| Radiergummi | Borrar goma |
| Korken      | Corcho      |