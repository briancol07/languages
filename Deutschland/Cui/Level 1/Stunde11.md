# Stunde 11 


| wie ist der Artikel?                                | kontrollfrage(W-frage)    | akkusativobjekt                   | Subjekt                                  |
| --------------------------------------------------- | ------------------------- | --------------------------------- | ---------------------------------------- |
| ich bestelle einen orangesaft                       | was bestelle ich?         | einen O-saft                      | Wer bestellt einen O-saft?(ich)          |
| Wie essen eine Pizzas                               | was essen wir?            | eine Pizza                        | Wer isst eine Pizza?(Wir)                |
| Sie trinkt manchmal einen Milchkaffe                | Was trinkt sie ?          | einen Milchkaffe                  | Wer trink manchmal einen Milchkaffe(Sie) |
| Nimmst du einen Rotwein oder einen Weißwein         | was nimmst du?            | einen Rotwein oder einen Weißwein | Wer nimmt einen R.oder einen w.?         |
| Er isst sehr selten --- Pommes                      | was isst er sehr selten ? | Pommes                            | Wer isst sehr selten Pommes? (er)        |
| Bestellt ihr auch einen Hamburguer und einen Döner? | Was bestellt ihr auch ?   | Einen Hamburguer & einen Döner    | Wer bestellt auch einen H. und einen D.? |