# Stunde 1 

## Personalpronomen

| palabra | Definicion |
| :-----: | :--------: |
|   ich   |     yo     |
|   du    |  vos , tu  |
| er/sie  | el / ella  |

<br>

<br>

| Frase       | Significado       |
| ----------- | ----------------- |
| und du?     | y vos?            |
| Guten Abend | Buenas tardes     |
| ich heiße   | yo soy / me llamo |

<br>

## Wir spiele den dialog 
<br>
A: Guten Abend <br>
B: Guten Abend, Wie heißt du ? <br>
A: ich heiße (Nombre) und du ? Wie heißt du ? <br>
B: ich Bin  (Nombre) <br>
A: Freut Mich! 
<br>
<br>

| .      | sein | heißen | kommen (aus) | wohnen (in) |
| ------ | ---- | ------ | ------------ | ----------- |
| ich    | bin  | heiße  | komme        | wohne       |
| du     | bist | heißt  | kommst       | wohnst      |
| er/sie | ist  | heißt  | kommt        | wohnt       |

sein = ser / estar = irreguläres Verb 
heißen,kommen , wohnen = reguläres Verb 

## Dialog 

> Ich heiße Julia Ohlendorf. Mein Vorname ist Julia . Mein Nachname/Familienname ist Ohlendorf. Ich bin Frau Ohlendorf. Ich bin *nich* Herr Ohlendorf . Ich komme aus Deutschland, aus Sangerhausen. Aber ich wohne in Argentinien, in Buenos Aires, in la Boca . 

--------
<br>

| -               | Definicion       |
| --------------- | ---------------- |
| Wer ist das?    | Quien es?        |
| Das ist         | el / ella es     |
| Vorname         | Nombre           |
| Nachname        | Apellido         |
| Österreich      | Austria          |
| Woher kommst du | De donde vienes? |
| Wo Wohnst du ?  | Donde vives?     |

-----

## Wir spiele den dialog 
<br>
A: Wie heißt du ? <br>
B: ich heiße (Nombre) und du ? Wie heißt du ? <br>
A: ich heiße (Nombre)  <br>
B: Woher kommst du?  <br>
A: ich komme aus (pais) und du ? <br>
B: Ich komme aus (pais) wo wohnst du ? <br>
A: Ich wohne in (lugar) und du ?<br>
B: Ich wohne in (lugar)

<br>

## Konjugation 

| -        | sein | heißen | kommen | wohnen | schreiben | buchstabieren
| -------- | ---- | ------ | ------ | ------ | --------- |--------------
| ich      | bin  | heiße  | komme  | wohne  | shreibe   | buchstabiere
| Du       | bist | heißt  | kommst | wohnst | schreibst | buchstabierst
| er / sie | ist  | heißt  | kommt  | wohnt  | schreibt  | buchstabiert
| Sie      | sein | heißt  | kommen | wohnen | schreiben | buchstabieren

<br>

Dast ist (ihrnen partner / ihre partnerin) er/sie heißt (nombre) er/sie kommt aus (lugar) . er/sie wohnt in (lugar) 

wie heißen Sie (usted) distinto de wie heißt sie (ella)

* Alberto ist argentinier , Pilar ist argentinierin 
* ich bin lehrein (profesora)
* ist da ein lehrer ? (profesor)

| Palabra       | Significado |
| ------------- | ----------- |
| Mahle         | moler       |
| Nacht         | noche       |
| Hauchen       | soplar      |
| Buchstabieren | deletrear   |

* wie buschstabieren man das ? (como se deletrea)
* wie schreibt man das ? (como se escribe)









