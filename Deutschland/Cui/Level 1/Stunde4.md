# Stunde 4 

wie viele fehler? 
Bitte 
Tschüss 

Was ---> Se lee vas 

vorherige 
nächste 
diktat (dictado)
zusammen schreiben 

| Die Interpunktion         | symbol |
| ------------------------- | ------ |
| Das ist ein punkt         | .      |
| Das ist ein Fragezeichen  | ?      |
| Das is ein Ausrufezeichen | !      |
| Das ist ein binderstrich  | -      |
| Das ist das Komma         | ,      |
| Das ist der Doppelpunkt   | :      |

<br>

## Hobbies 

etwas „gern/nicht gern machen“ - algo „me/te/le/nos/les“ gusta hacer“
Was machst du gern? Was machst du nicht gern? (eine generelle Frage/una pregunta
generalizada)
ANTWORT: spezielles Verb (NICHT: Ich mache gern lachen. 
Ich lache gern. / Ich mache gern Sport.

Die komödien (Die komödie)
Augmöden (hobbys)
welche hobbz kennen wie (Que hobbies conocen)

* Hobby 
  * Spielen piano 
  * Dancen 
  * Lessen (leer)
  * Kochen 
  * kayak fahren 
  * Reisen 
  * Yoga Machen 
  * Gan Machen 
  * Schwimmen 

* Ich lache gern. (lachen) 
* Ich gehe gern tanzen. (tanzen gehen)
* Ich schwimme gern. (schwimmen) 
* Ich gehe gern shoppen. (shoppen gehen)
* Ich reise gern. (reisen) 
* Ich gehe gern ins Kino. (ins Kino gehen)
* Ich fotografiere gern. (fotografieren)
* Ich gehe gern ins Restaurant. (ins Restaurant gehen)
* Ich tanze gern. (tanzen) 
* Ich spiele gern Computerspiele. (Computerspiele spielen)
* Ich shoppe gern. (shoppen)

> „gern“ funktioniert es auch, aber andere Intention. (Hago algo, pero no significa necesariamente q
me gusta.) Gern = Adverb (Nur für verben!)

VERBKONJUGATION KOMPLETT + 1. Person, 2. Person, 3. Person Plural, regelmäßige Verben

| VERBKONJUGATION            | heißen | kommen (aus) | schwimmen | gehen | tanzen | trinken |
| -------------------------- | ------ | ------------ | --------- | ----- | ------ | ------- |
| ich                        | heiße  | komme        | schwimme  | gehe  | tanze  | trinke  |
| du                         | heißt  | kommst       | schwimmst | gehst | tanzt  | trinkst |
| er/sie/…/ (tercer persona) | heißt  | kommt        | schwimmt  | geht  | tanzt  | trinkt  |
| wir                        | heißen | kommen       | schwimmen | gehen | tanzen | trinken |
| ihr                        | heißt  | kommt        | schwimmt  | geht  | tanzt  | trinks  |
| sie (ellas/ellos)          | heißen | kommen       | schwimmen | gehen | tanzen | trinken |
| Sie(Usted/Ustedes)         | heißen | kommen       | schwimmen | gehen | tanzen | trinken |

julia sing gern (le gusta cantar )
was machen deutsche gern 
was machen deutsche 

in der freizeit (En su tiempo libre)
und was machen sie noch gern (Que mas le gusta hacer)
<br>

* was machen Argentiner 
  * Asado machen 
  * Mate trinken 
  * Shoppen gehen 
  * Reisen 
  * Empanadas Essen 
  * Warten Nicht gern 

sie fahren nicht im öffentichen nahuer kehr(öpnv)

was mach dir 
wer geht ins kino 

ich lache gern (lachen = risa)
ich schwimme gern
ich reise gern 
ich fotografiere gern 
ich gehe gern tanzen 
ich gehe gern shoppen 
ich gehe gern ins kino 
ich gehe gern ins restaurant 
ich spiele gern computerspiele
ich mache gern sport 
