# Kanji 漢字

> Book : Write Now (kanji for beginners)

## Chapter 1　

Kanji | kunyomi | Onyomi  | Description | Use  
------|---------|---------|-------------|---------------
日    |ひ　/　び / か |にち　/　に　/　にっ  | Sun , Day | 日よう日 - 15日
月　　|つき |　げつ  　がつ | Moon , Month |　月よう日 - 3 月
火　  | ひ　| か　| Fire |  火よう日- 火　
水　　| みず| すい　| Water | 水よう日- 水　
木　  |き　|　もく　| Tree, Wood | 木よう日-木
金　  |かね|きん　  | Gold, Metal , Money |　金よう日 -お金   
土　　|　  |ど    　|　Ground , Soil , Earth |　土よう日 
山　  |やま |さん　　|　Mountain |山-
川   |かわ　/　がわ  |  | River  |　川
田 　| た　/　だ　|   |  Rice Field , Paddy 
人 　| ひと| じん　/　にん| Person | 人-日本人

### Sentences 


## Chapter 2 

Kanji | kunyomi | Onyomi  | Description | Use  
------|---------|---------|-------------|---------------
   口 |   くち /　ぐち  |こう　 |  Mouth, Opening | 口　で口　
　目  | め     |         |  Eye, item      |目　にん目
 耳   | みみ   |         | Ear     |　耳
 手 　| て     |  しゅ   | Hand            |　手がみ　手


## Chapter 3

Kanji | kunyomi | Onyomi  | Description | Use  
------|---------|---------|-------------|---------------
      |         |         |             |

## Chapter 4
Kanji | kunyomi | Onyomi  | Description | Use  
------|---------|---------|-------------|---------------
     |         |         |             |

## Chapter 5
Kanji | kunyomi | Onyomi  | Description | Use  
------|---------|---------|-------------|---------------
     |         |         |             |

