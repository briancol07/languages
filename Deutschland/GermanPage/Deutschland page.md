# Aleman / German

> Block de notas amedida que voy aprendiendo aleman . Usare esta pagina como guia , luego complementare con otra actividad y otras paginas .

* [GermanPage](https://learngerman.dw.com/en/abc/c-39621991)



## ABC 

### A 

Word | Definition 
-----|------
Ananas |
Stift | Pen 
Paprika |Bell Pepper 
<br>
Die 
  
### N 

Word | Definition 
-----|------
Nase| Nariz
Banane| Banana 

### I 

Word | Definition 
-----|------
Musik | musica
information| informacion

### S 

Word | Definition 
-----|------
Salat| Ensalada
Dose| can 
Siehst|

### U 
Word | Definition 
-----|------
Uhr| hora 
Mund | Boca
Gut | Bien 
Null| Cero 
Rund | Redondo 

>Wie viel uhr ist es ? <br>
>Der mund ist rot und rund 

>Que hora es? <br>
>His mouth is red and round . 

### T 

Word | Definition 
-----|------ 
Brot | Pan
Tomate | tomate
Acht | 8
interessant | interesante 
trinken | Beber 

>ich möchte bitte 8 tomaten und ein brot .<br>
Hier ist das brot , aber wir haben nur eirne tomate . <br>
>me gustaria 8 tomates y un pan <br>
Aca esta el pan pero no tenemos tomate 

> Das brot schneckt gut
> <br> Ese pan sabe bien 

### E 

Word | Definition 
-----|------ 
Esel | burro 
Ente | Duck 
Elegant| elgante
Lessen | leer 
Eltern | padres 

> Uvan wie vicle ente siehst du ? 
> <br> ich she eine ente 

> Cuantos patos ves? <br>
> Veo un pato 

> Im Tierpark kann man einen esel shen ? 
> <br> Zoologico 

### D 

Word | Definition 
-----|------ 
Dose | lata
schokolade | chocolate
Denken | pensar 
Drei | 3 
Danke | Gracias 

> Hast du eine dose ? 
> <br> Ja, ich habe drei dosen für dich 

> Tiene una lata? <br>
> yug ,  tengo 3 latas para ti 

> ich mag dunkle schokolade 
> <br> Me gusta el chocolate oscuro 

### O 

Word | Definition 
-----|------ 
olive | oliva 
hose | pantalones 
Ordnen | organizar 
Oft | amenudo 
fotografieren | foto 

> Oh luan , das ist die letzle olive <br>
> Okay , ich hole noch mehr oliven 

> Oh luan esta es la ultima oliva
> Traere mas aceitunas 

> mels hose ist toll 
> <br> Los pantalones de mel son geniales 

### M 


Messer | knife
lampe| lamp 
montieren | armar 
mund | boca 
Umarmern| abrazo 

> Mel , soll ich die lampe anmachen? 
> <br> ja, Luan , mach bitte die lampe an! 

> Deberias encender la lampara , si luan enciendela 

> ich möchte mit meinem messer tomaten shneiden 

> Quiero cortar tomate con mi cuchillo

### H 
Word | Definition 
-----|------ 
hose | pantalones 
hand| mano
haus|casa 
Sehen | ver
gehen | vamos 
<br>

>wie viele finger hat eine hand 
> <br> ein hand hat  5 finger 

> Cuantos dedos tiene la mano ?<br> 
> La mano tiene 5 dedos 

> ich kaufe heute eine hose 
> <br> hoy me compro unos pantalones 

### L 

Word | Definition 
-----|------ 
Polizist | policia
gelb | amarillo 
helfen | ayudan
<br>

> Luan maltiene gelbe lampe 
> <br> Was bist du von beruf 

>Ich bin seit 11 jahren polizist 
><br> A sido policia por 11 años


### F 

Word | Definition 
-----|------ 
Fisch | Fish
löffel | Spoon
Fragen | Pedir 
Schalafen | Dormir 
fünf | 5 

> Brauchen wir löffel 
> Ja , wir braunchen fünf löffel
> 
> Necesitan cucharas ? <br>
> Si Necesitamos 5 cucharas 

> Fritz isst keinen frishohen fish . 

### Pf 

Word | Definition 
-----|------ 
Pfannen | Sarten 
Apfel | Manzana
Kopf | Cabeza 
Tropfen | Gotas 
Pflegen | Preocuparse 

> Mel , möchtest du auch einen apfel 
> Nein , ich möchte einen pfrisich essen 

> Mel Te gustaria una manzanza ? 
> <br> No quiero un melocoton 

> Die pfanne steht auf dem herd 
> <br> La Sarten en la estufa 

### V 
> Aveces la V la pronuncian como F 

Word | Definition 
-----|------ 
Vogel | Pajaro 
Pullover | pullover 
Vier | Cuatro 
Verstehen| Understand 
Vorname|  First name 

> Mel , wie viele pullover nimmst di ?
> <br> Mit in den urlaub? 
> <br> Ich packe nur einen pullover ein 

> Mel , Tiene muchos pullovers 
> Cuantos busos te llevates de vacaciones 
> yo , solo empaque uno solo pullover 

> Ein vogel singt 
><br> El pajaro canta 

### W

Word | Definition 
-----|------
Krawatte | Corbata
Wolle |  Lana
Zwölf |  12 
Wasser | Agua 
Wohnen | living 

> Kaufst du eine krawatte ? 
> <br> Nein , ich kaufe zwölf krawatten 
> >
> Die wolle ist weich  


### G 

Word | Definition 
-----|------
Geld | Money 
Igel | Erizo 
Wolle| lana 
Gelb | Yellow 
Gehen| Go/Going 
Springen| Jumping 

> Wie viel geld hat mel? 
> <br> Mel hat genan 10 euro 
> 
> Dieser igel ist ganz grob! 

### K 

Word | Definition 
-----|------
Koffer | Suitcase 
Shokolade| Chocolate
Kaufen | 
Danke | Ty 

> Kauft luan schokolade 
> <br> klar , er kauft schokolade undeinen 
> <br> kleinen Koffer 

> Danke , die schokolade schmect lecker 

### Ck  

Word | Definition 
-----|------ 
Rock | Pollera
Rucksack | Backpack 
Gucken | Look 
Decke | cover/Blanket
Backen | bake 

Ist der rock neu
ja, der rock ist neu 

Der Rucksack ist sehr grob 
his backpack is  big 

### C 
Word | Definition 
-----|------
Computer|
Creme| 
Cent|
Cd|
Cafe|
Campen|
Cool | 

Hast du einen computer luan?
ja , ich habe einen computer 

### CH 

Word | Definition 
-----|------ 
Milch| leche 
Buch | Book 
Chef|
Öfftentrich| 
Becher | 
Leicht |
Kochen| 
Besuch| 
Lachen |

ich trinke einen becher milch 
mel , ist das dein buch ?
Ja, luan das ist ein buch über enten 

### SCH

Word | Definition 
-----|------
Schokolade| 
Flasche | Botella
Schlafen |
Deutsch | 
Schnell|

Kaufst du eine flashe wasser 
Ja, ich kaufe eine flashe wasser 
im supermarket

Die schokolade schmeckt gut 

### St 
Word | Definition 
-----|------
Stuhl | silla 
Stift | pen 
Streiten |
Stehen | 
Still | 

Der stuhl steht in der ecke
Mel mas du einen stift ?
Ja , ich habe sogar 8 stiffte 

### SP 

Word | Definition 
-----|------
Spiegel|Espejo 
Spinne | Spider 
Spät | 
Sprechen | 
Springen | 

das kind schaut in den spiegel 
Guck mal , luan da ist eine spinne
oh ich mag keine spinnen 


 




