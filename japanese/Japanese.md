# Japanese 

## First we have hiragana (ひらがな)

  |  |　|K|S|T|N|H|M|Y|R|W|N|G|Z|D|B|P
--|-|--|-|-|-|-|-|-|-|-|-|-|-|-|-|-
A|あ|か|さ|た|な|は|ま|や|ら|わ|ん|が|ざ|だ|ば|ぱ|
I|い|き|し|ち|に|ひ|み|  |り| | |ぎ|じ|ぢ|び|ぴ|
U|う|く|す|つ|ぬ|ふ|む|ゆ|る| | |ぐ|ず|づ|ぶ|ぷ|
E|え|け|せ|て|ね|へ|め|  |れ| | |げ|ぜ|で|べ|ペ|
O|お|こ|そ|と|の|ほ|も|よ|ろ|を| |ご|ぞ|ど|ぼ|ぽ|

##  Second we have Katakana (カタカナ)

 |  | |K |S|T|N|H|M|Y|R|W|N|G|Z|D|B|P
-|--|--|-|-|-|-|-|-|-|-|-|-|-|-|-|-|
A|ア|カ|サ|タ|ナ|ハ|マ|ヤ|ラ|ワ|ン|ガ|ザ|ダ|バ|パ
I|イ|キ|シ|チ|ニ|ヒ|ミ|　|リ|　|　|ギ|ジ|ヂ|ビ|ピ
U|ウ|ク|ス|ツ|ヌ|フ|ム|ユ|ル|　|　|グ|ズ|ヅ|ブ|プ
E|エ|ケ|セ|テ|ネ|ヘ|メ|　|レ|　|　|ゲ|ゼ|デ|ベ|ペ
O|オ|コ|ソ|ト|ノ|ホ|モ|ヨ|ロ|ヲ|　|ゴ|ゾ|ド|ボ|ポ

## Minna no nihongo ( みなの日本語 )
----
### Chapter 1 
----
#### 　れんしゅう　A 

1. 
   1. わたし は  かいしゃいん です。

   2. やだまだ さん  は  いしゃ です。  
   
2. 
   1. わたし　は　アメリカ　じん　しゃ　ありません。 
   
   2. イーさん　は　がくせい　では　　ありません。
   
3. 
   1. あの　ひと（かた）　は　マリアさん　ですか。
   
   2. あの　ひと　は　だれ（どなた）ですか。
   
4. 1. わたし　は　IMC　の　しゃいん　です。
   
   2. カリナさん　は　さくらだいがく　の　せんせい　です。
   
5. 1. サントス　は　ブラジル　じん　です。
   
   2. マリア　さん　も　ブラジルじん　です。
   
   3. あの　ひと　も　ブラジルじん　です。
   
6. 1. テレーザ　ちゃん　は　きゅさい　です。
   
   2. サントス　は　なんさい（おいくつ）ですか。 

### Explanation 

1.  A  は　B です
> This try to said that A is B , **は** is a topic particle 

2. > This continue with that logic but now he add   **しゃ　ありません**  , this say A is not B . 

3. > This are questions , because they end with a **か**
    and they asking that person is maria or who is that person 
4. >  Here the add Particle **の** , this says that he is something from the other example **だいがく　の　せんせい**　, he is a teacher in the university .

5. > **じん** is to reffer people from that country  , then he add a new Particle : **も**　it like also 
サントス　and　マリア are from the same country .

6. > this is age topic there some special ones like.
   we ask : **なんさいですか** the correct way , **おいくつ　ですか**　　



----
### Chapter 2 
----

#### 　れんしゅう　A 

1. 
   * これはつくえです。
   * これはなんですか。

2. * それは一ですか、ななですか。
   * 其れは　ぼールペン　ですか、シャープペンシル　ですか。

3. * これは　くるま　の　ほんです。
   * これは　なん　の　ほんですか。

4. * あれは　わたし　の　かばんです。
   * あれは　だれのかばん　ですか。

5. * あれは　せんせい　の　です。
   * あれはだれのですか 。

6. * この　てちょう　は　わたしのです。
   * この　かぎ　は　わたしのです。

----
### Chapter 3 
----

#### 　れんしゅう　A 
   
   1. * ここは　だいがく　です
      * ここは　ひろしま　です
  　
  2. * うけつけ　ここ　です
     * うけつけ　どこ　ですか
  
  3. * じどうはんばいきは　２かいです
     * さとうさん　は　じむじょ
     * さとうさんは　どこですか
  
  4. * エレベーター　は　こちら　です 
     * エレベーター　どちらですか
  
  5. * くに　は　フランス　です
     * かいしゃ　は　IMC です
     * だいがく　は　さくらだいがく
     * どちらですか
  
  6. *  これは　にほん　の　くるまです
     *  どこ　の　くるまですか

  7. * このネクタイは　１５００えん　です　
     * この　ネクタイ　は　いくらですか

 



