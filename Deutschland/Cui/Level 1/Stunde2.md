# Stunde 2 

wie derholung --- > Repetir 

wie heißen du  (Informell) 
wie heißen Sie (Formell)

Auch ich komme aus ....

herr kelle
Frau  (mujer)


| Frases            | Significado                        |
| ----------------- | ---------------------------------- |
| wie geht's dir?   | Como estas? (INformell)            |
| wie geht es ihnen | Como estas (Formell)               |
| Gutten Morgen     | Buenos dias                        |
| Gutten tag        | Buen dia                           |
| Gutten abend      | Buenas tardes                      |
| Gutten Nacht      | Buenas noches (solo irse a dormir) |

## Respuestas a wie geht's dir ? 

* Danke 
    * Sehr Gut , Super 
    * Gut
    * OK 
    * Nich Gut 

## Ich mag.... (me gusta)

* Fahrrad Fahren (andar en bicileta)
* Singen / imchor singen  
* ich mag Beides  (me gustan ambas cosas )
* Schokolade oder chips 

Was Mögen (opcion) Sie ----> her/frau
Was magst (opcion) Due 

---------

Brot (pan)
essen (comer)
Reisen (viajar)
Die Natur (la naturaleza)
Der vegetarier 
tee 
kaffee

## Wer ist das?  -----> Que/Quien es / eso?

* Rta 
  * Das ist peter 
  * er kommt aus Hamburg und mag kaffee
* was ist das?
* Wei heiß das?


## Artikel 

| Articulos | Tipo      |
| --------- | --------- |
| Die       | Femenino  |
| Der       | Masculino |
| Das       | Sin       |

<br>

| Articulo | Palabra     | Significado        |
| -------- | ----------- | ------------------ |
| Die      | Tatshe      | Bolso              |
| Die      | Flashe      | Botella            |
| Der      | Schlüssel   | Llave              |
| Das      | Heft        | Cuaderno           |
| Das      | Buch        | Book               |
| Die      | Brille      | Lentes             |
| Das      | Foto        | Foto               |
| Der      | Spiegel     | Espejo             |
| Die      | Zahnbürste  | Cepillo de dientes |
| Die      | Zigarette   | Cigarrillo         |
| Der      | Lippenstift | Pinta labio        |
| Das      | Tashetuch   | Carilina           |
| Die      | Zeitung     | Diario             |
| Das      | Portmonee   | Billetera          |
| Die      | Richterin   | Juez               |
| Der      | Mundschutz  | Protector bucal    |
| Die      | Serie       | Serie              | 
| Die      | Kopfhörer   | Auriculares        | 
| Die      | Pause       | Pausa              | 
| Die      | Lehrer      | Profesor           |
--------------

Was ist in der tasche ? (Que hay en el bolso)
Was ist in Der Rucksack ? 

In Mein hand tashe 
Das Notizbuch 
Jura-buch

--------------

Ich weiß nicht   (No se)
Ich Weiß es nicht (No lo se )
Ich bin in der Pause (Estoy en el receso)
Wer Fänt an?    (Quien empieza)
ich Fange an (emiezo Yo)
Wer antwortet   (quien responde)
würfel (cubo/dado)
aber (but)
s